using RPGCharacters.Character;
using System;
using Xunit;
namespace RPGCharacters_Test
{
    public class LevelUp_Test
    {

        [Fact]
        public void Test_LevelUp_Mage_Calculating_The_Damage_Value()
        {
            Mage mageCharacter = new Mage("Mage");
            mageCharacter.LevelUp();
            Assert.Equal(8, mageCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(2, mageCharacter.PrimaryAttributes.Strength);
            Assert.Equal(2, mageCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(13, mageCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(80, mageCharacter.SecondaryAttributes.Health);
            Assert.Equal(4, mageCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(13, mageCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(6.5, mageCharacter.Damage);
        }

        [Fact]
        public void Test_LevelUp_Ranger_Calculating_The_Damage_Value()
        {
            Ranger rangerCharacter = new Ranger("Ranger");
            rangerCharacter.LevelUp();
            Assert.Equal(10, rangerCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(2, rangerCharacter.PrimaryAttributes.Strength);
            Assert.Equal(12, rangerCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(2, rangerCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(100, rangerCharacter.SecondaryAttributes.Health);
            Assert.Equal(14, rangerCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, rangerCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(12.8, rangerCharacter.Damage);

        }

        [Fact]
        public void Test_LevelUp_Rogue_Calculating_The_Damage_Value()
        {
            Rogue rogueCharacter = new Rogue("Rogue");
            rogueCharacter.LevelUp();
            Assert.Equal(11, rogueCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(3, rogueCharacter.PrimaryAttributes.Strength);
            Assert.Equal(10, rogueCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(2, rogueCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(110, rogueCharacter.SecondaryAttributes.Health);
            Assert.Equal(13, rogueCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, rogueCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(11.4, rogueCharacter.Damage);
        }

        [Fact]
        public void Test_LevelUp_Warrior_Calculating_The_Damage_Value()
        {
            Warrior warriorCharacter = new Warrior("Warrior");
            warriorCharacter.LevelUp();
            Assert.Equal(15, warriorCharacter.PrimaryAttributes.Vitality);
            Assert.Equal(8, warriorCharacter.PrimaryAttributes.Strength);
            Assert.Equal(4, warriorCharacter.PrimaryAttributes.Dexterity);
            Assert.Equal(2, warriorCharacter.PrimaryAttributes.Intelligence);
            Assert.Equal(150, warriorCharacter.SecondaryAttributes.Health);
            Assert.Equal(12, warriorCharacter.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, warriorCharacter.SecondaryAttributes.ElementalResistance);
            Assert.Equal(11.4, warriorCharacter.Damage);

        }
    }
}
