﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.Aspects;

namespace RPGCharacters.weapon
{
    public class Item
    {
        public int ItemLevel { get; set; }

        public string ItemName { get; set; }

        public Slots ItemSlot { get; set;  }

    }
}
