﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.weapon
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public override string Message => "Wrong type of the weapon or hero level not high enough";
    }
}

