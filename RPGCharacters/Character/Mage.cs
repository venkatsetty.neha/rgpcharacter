﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.Aspects;
using RPGCharacters.weapon;

namespace RPGCharacters.Character

{
    public class Mage : Character
    {

        /// <summary>
        /// Child class constructor Extends Base class constructor to Create the Character
        /// Invokes the Secondary Attributes method from baseclass
        /// </summary>


        public string weaponSelected = "";
        public Mage(string Name) : base(Name)
        {
          
           
            PrimaryAttributes.Strength = 1;
            PrimaryAttributes.Dexterity = 1;
            PrimaryAttributes.Intelligence = 8;
            PrimaryAttributes.Vitality = 5;

            SecondaryAttributesCalculations();

            Console.WriteLine($" YOU HAVE SELECTED  {Name} AND THE LEVEL IS {Level} ");

            Console.WriteLine($" THE EQUIPPED POWERS ARE : ");

            Console.WriteLine($"  Strength:{PrimaryAttributes.Strength} ");
            Console.WriteLine($"  Dexterity:{PrimaryAttributes.Dexterity}  ");
            Console.WriteLine($"  Intelligence:{PrimaryAttributes.Intelligence}"); 
            Console.WriteLine($"  Vitality:{PrimaryAttributes.Vitality} ");

            Console.WriteLine($"  Health:{SecondaryAttributes.Health} ");
            Console.WriteLine($"  ArmorRating:{SecondaryAttributes.ArmorRating} ");
            Console.WriteLine($"  ElementalResistance:{SecondaryAttributes.ElementalResistance} " );
            Console.WriteLine($"  Damage: {Damage}");

            Console.WriteLine($"NOW CHOOSE THE WEAPON FOR CHARACTER : {Name} ");

            Console.WriteLine($" FOR MAGES – STAFFS OR WANDS ");


            ///< summary >
            ///  The weapons can be selected that is assigned to the Charater
            ///  Gives Exception if wrong Weapon is Selected
            ///  Invokes the Secondary Attributes method from baseclass
            ///  Levelups after selecting a weapon
            /// </summary>

            string weaponsName = Console.ReadLine().ToString().ToLower();

            if (weaponsName == "staffs" || weaponsName == "wands")
            {
                weaponSelected = weaponsName;
                Console.WriteLine(Name + " chossed weapon:" + weaponsName);
                LevelUp();
            }
            else
            {
                throw new InvalidWeaponException();
            }

        }
        /// <summary>
        /// Overrides the Baseclass LevelUp method to go to Character next level 
        /// Invokes the SecondaryAttributesCalculation Method
        /// Displays the Damage Occured
        /// </summary>
        /// <param name="level"></param>
       
        public override void LevelUp(int level = 1)
        {
            base.LevelUp(level);
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 1;
            PrimaryAttributes.Intelligence += 5;
            PrimaryAttributes.Vitality += 3;
            SecondaryAttributesCalculations();

            calculateDamage(weaponSelected);

            Console.WriteLine($" YOU HAVE SELECTED  {Name} AND THE LEVEL IS {Level} ");

            Console.WriteLine($"  Strength:{PrimaryAttributes.Strength} ");
            Console.WriteLine($"  Dexterity:{PrimaryAttributes.Dexterity}  ");
            Console.WriteLine($"  Intelligence:{PrimaryAttributes.Intelligence}");
            Console.WriteLine($"  Vitality:{PrimaryAttributes.Vitality} ");

            Console.WriteLine($"  Health:{SecondaryAttributes.Health} ");
            Console.WriteLine($"  ArmorRating:{SecondaryAttributes.ArmorRating} ");
            Console.WriteLine($"  ElementalResistance:{SecondaryAttributes.ElementalResistance} ");
            Console.WriteLine($"  Damage: {Damage}");

        }

        /// <summary>
        /// Method to Calculate Damage from the Character Class
        /// </summary>
        /// <param name="weaponSelected"></param>

        public void calculateDamage(string weaponSelected)
        {
            double WeaponDPS = CalculateWeaponDPS(weaponSelected);
            Damage = WeaponDPS * (1 + (PrimaryAttributes.Intelligence / 100));
        }
    }

   }
